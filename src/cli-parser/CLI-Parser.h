#ifndef PARSER_H_
#define PARSER_H_

#include "work-pool/PoolManager.h"


namespace DFT
{

class Parser 
{
    
public:
    static int parse(PoolManager& manager, int& argc, char *argv[]);

};

}
#endif // PARSER_H_