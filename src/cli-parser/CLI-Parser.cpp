#include "CLI-Parser.h"
#include <boost/program_options.hpp>
#include "work-pool/PoolManager.h"
#include "enums/eStoppingCondition.h"
#include "logging/logger.h"


namespace DFT
{

int Parser::parse(PoolManager& manager, int& argc, char *argv[]){
    // if(argc<2)
    // {
    //     printf("Too few arguments\n");
    // }
    // else
    // {
    int returnStatus = 0; //Error free return, run decoded instructions
    try {
        namespace po = boost::program_options;
        po::options_description desc("Options");
        desc.add_options()
            ("input-file,I", po::value<std::string>(), "Path to the model file. Requidered.")
            ("time-bound,T", po::value<double>(), "Time bound for the individual simulations. Requidered.")
            ("workers,W", po::value<int>()->default_value(0) ,"Number of Workers/Threads to run simulations. Positive int to specify total number of workers, 0 for automatic (detect number of threads present), negative to leave that number of threads free. Defaults to automatic.")
            ("ci-percentage", po::value<double>()->default_value(95.0), "Confidence Interval percentage. Double between 0 and 100, excluding 0 and 100. Default 95.0")
            ("stopping-condition,S", po::value<std::string>(), "Stopping condition for the simulations. Requidered. Valid: 'samples','S','time','T','confidenceinterval','CI'.")
            ("target-samples,N", po::value<int>(), "Number of samples to generate, positive non-zero long integer. Requidered when using sample stopping condition.")
            ("run-time,R", po::value<int>(), "Total time to run simulation for in seconds. Requidered when using time stopping condition.")
            ("ci-width,CI", po::value<double>(), "Width of the confidence interval that must be reached before stopping the simulations. Requidered when using CI stopping condition.")
            ("ci-algorithm", po::value<std::string>()->default_value("Wilson"), "Which algorithm to use for calculating the binomial Confidence Interval. Valid: 'Normal', 'Wilson'")
            ("debug", po::value<bool>()->default_value(false), "Print debug info. Boolean.")
            ("seed", po::value<unsigned int>(), "Random seed for the simulations, unsigned integer, 0 = no fixed random seed")
            ("run-tests", "Run the internal functionality test suite")
            ("help,?", "Show this help message");
        po::positional_options_description p;
        p.add("input-file", -1);
        
        po::variables_map vm;
        po::store(po::command_line_parser(argc, argv).
            options(desc).positional(p).run(), vm);
        po::notify(vm);

        // std::cout << "c parsed: "  << vm.count("c") << "\n";
        // std::cout << "d parsed: "  << vm.count("d") << "\n";
        // std::cout << "filename: '" << boost::any_cast<std::string>(vm["filename"].value()) << "'\n";

        //Special case, no arguments given: show help
        if (argc<2) {
            std::cout << "INFO: No arguments given, displaying help message." << std::endl;
            std::cout << desc << "\n";
            returnStatus = 1;
        //Special case, help argument entered: only show help
        } else if (vm.count("help")) {
            std::cout << desc << "\n";
            returnStatus = 1;
        
        //special case, run tests argument entered: only run tests
        } else if (vm.count("run-tests")) {
            std::cout << "INFO: Running test suite." << std::endl;
            Logger::logInfo("INFO: Running test suite.");
            returnStatus = 7357; //Run tests return code

        // Default behavior
        } else {
            //check input file is given
            if ((vm.count("input-file"))) {
                manager.setFile(boost::any_cast<std::string>(vm["input-file"].value()));
            } else {
                throw std::invalid_argument("ERROR: No input file specified, use -? to see the program options.");
            }

            //check valid time bound is given
            if (!(vm.count("time-bound"))) {
                throw std::invalid_argument("Unspecified time bound.");
            } else if (boost::any_cast<double>(vm["time-bound"].value()) <= 0.0) {
                throw std::invalid_argument("Negative or zero time bound, must be positive non-zero.");
            } else {
                manager.setTimebound(boost::any_cast<double>(vm["time-bound"].value()));
            }
            
            manager.setWorkerCount(boost::any_cast<int>(vm["workers"].value()));
            
            manager.setCIPercentage(boost::any_cast<double>(vm["ci-percentage"].value()));

            //Check a stopping condition is given
            if (!(vm.count("stopping-condition"))){
                throw std::invalid_argument("no stopping condition defined.");
            } else {
                DFT::eStoppingCondition stopingCondition = DFT::parseStoppingCondition(boost::any_cast<std::string>(vm["stopping-condition"].value()));
                
                manager.setStoppingCondition(stopingCondition);

                //Check if a stopping argument matching the specified stopping condition is given
                switch (stopingCondition){
                    case DFT::eStoppingCondition::eTime:
                        if (!(vm.count("run-time"))){
                            throw std::invalid_argument("no run time defined with stopping condition run time.");
                        } else {
                            manager.setStoppingTime(std::time(nullptr) + boost::any_cast<int>(vm["run-time"].value()));
                        }
                        break;

                    case DFT::eStoppingCondition::eSamples:
                        if (!(vm.count("target-samples"))){
                            throw std::invalid_argument("no number of samples defined with number of samples stop condition.");
                        } else {
                            manager.setTargetSamples(boost::any_cast<int>(vm["target-samples"].value()));
                        }
                        break;

                    case DFT::eStoppingCondition::eCI:
                        if (!(vm.count("ci-width"))){
                            throw std::invalid_argument("no CI stopping width defined with CI-width stopping condition");
                        } else {
                            manager.setTargetCI(boost::any_cast<double>(vm["ci-width"].value()));
                        }
                        break;

                    default:
                    throw std::runtime_error("unknown stopping condition: " + boost::any_cast<std::string>(vm["stopping-condition"].value()) + ". Valid: 'time','T','samples','S','confidenceinterval','CI'.");
                };

            };
            
            manager.setCIBAlgorithm(boost::any_cast<std::string>(vm["ci-algorithm"].value()));
            manager.setPrintDebug(boost::any_cast<bool>(vm["debug"].value()));
            
            if (vm.count("seed")){
                std::cout << "INFO: Specifying a seed will only generate consistent output when also using the same number of threads and sample count stopping condition" << std::endl;
                manager.setRandomSeed(boost::any_cast<unsigned int>(vm["seed"].value()));
            }
        }
    } catch(std::invalid_argument& e) {
        std::cerr << "ERROR: Invalid arguments; " << e.what() << std::endl;
        returnStatus = 1;
    } catch(std::exception& e) {
        std::cerr << "Error: " << e.what() << std::endl;
        returnStatus = 1;
    } catch(...) {
        std::cerr << "Unknown throw was caught!" << std::endl;;
        returnStatus = 1;
    }
    return returnStatus;
    
};

}
