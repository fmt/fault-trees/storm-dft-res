
#specify ConfidenceInterval library
add_library(cli-parser 
${CMAKE_CURRENT_SOURCE_DIR}/CLI-Parser.h
${CMAKE_CURRENT_SOURCE_DIR}/CLI-Parser.cpp)

#target_link_libraries(${PROJECT_NAME} PRIVATE confidence-interval)
target_link_libraries(cli-parser PRIVATE work-pool storm-dft logging)
