#ifndef PROGRESS_SPINNER_H_
#define PROGRESS_SPINNER_H_

#include <string>
#include <vector>
#include <mutex>
#include "enums/eWorkerStatus.h"

namespace DFT
{

class activitySpinner {

public:
activitySpinner();
void tick();
std::string toString() const;
std::string tickToString();
void setTickSymbols(std::vector<std::string> tickSymbols);
size_t getProgress() const;
void setProgress(size_t progress);
void setTrackingThread(std::vector<eWorkerStatus> &threads, size_t const id);


private:
std::vector<std::string> tickSymbols = std::vector<std::string>{"-", "\\", "|", "/"};
std::vector<eWorkerStatus>* trackedThreadsVector;
size_t threadID;
size_t progress;
std::mutex set_mutex;
eWorkerStatus getTrackedThreadStatus() const;







};

}


#endif