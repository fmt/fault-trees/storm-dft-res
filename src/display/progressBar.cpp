#include "progressBar.h"





namespace DFT
{

struct LineInsertingFilter : boost::iostreams::line_filter
{
    std::string do_filter(const std::string &line)
    {   //\n: generate next line/scroll down one line if it is on the bottom edge of the terminal
        //\033[A: move cursor up one line back to the start of the line and
        //\033[L: move current line down one line in the empty space that is now guarenteed to exist
        return "\n\033[A\033[L" + line;
    }
};


progressBar::progressBar() : 
    stdout_buf(std::cout.rdbuf()), 
    os(stdout_buf), 
    bar{indicators::option::Stream{os},
        indicators::option::BarWidth{40},
        indicators::option::Start{"["},
        indicators::option::Fill{"="},
        indicators::option::Lead{">"},
        indicators::option::Remainder{" "},
        indicators::option::End{"]"},
        indicators::option::PostfixText{"Progress"},
        indicators::option::ForegroundColor{indicators::Color::green},
        indicators::option::ShowElapsedTime{true},
        indicators::option::ShowRemainingTime{true},
        indicators::option::ShowPercentage{true},
        indicators::option::FontStyles{std::vector<indicators::FontStyle>{indicators::FontStyle::bold}}}
{
        // create a filtering_ostreambuf with our filter and the stdout streambuf as a sink
        filtering_buf.push(LineInsertingFilter());
        filtering_buf.push(*stdout_buf);
    //other steps on construction
};

progressBar::~progressBar() {
    stopDisplaying();
}

void progressBar::resetBar(){
        (&bar)->~ProgressBar();
        new (&bar) indicators::ProgressBar{
            indicators::option::Stream{os},
            indicators::option::BarWidth{40},
            indicators::option::Start{"["},
            indicators::option::Fill{"="},
            indicators::option::Lead{">"},
            indicators::option::Remainder{" "},
            indicators::option::End{"]"},
            indicators::option::ForegroundColor{indicators::Color::green},
            indicators::option::ShowElapsedTime{true},
            indicators::option::ShowRemainingTime{true},
            indicators::option::ShowPercentage{true},
            indicators::option::FontStyles{std::vector<indicators::FontStyle>{indicators::FontStyle::bold}}
        };
        updateBarPostfix();
};

void progressBar::updateBarPostfix(){
    switch (activeStoppingCondition) {
        case eStoppingCondition::eTime:
        bar.set_option(indicators::option::PostfixText{"Progress to stopping time."});
        break;
    case eStoppingCondition::eSamples:
        bar.set_option(indicators::option::PostfixText{"Progress through samples."});
        break;
    case eStoppingCondition::eCI:
        bar.set_option(indicators::option::PostfixText{"Progress to desired CI width."});
        break;
    default:
        bar.set_option(indicators::option::PostfixText{"Progress."});
    }
};

void progressBar::startDisplaying(){
    storedStartingTime = std::time(nullptr);

    if (!coutRediret){  //start the cout redirect if not yet active
        coutRediret = true; //set cout redirect flag
        
        std::cout.rdbuf(&filtering_buf); // configure std::cout to use our streambuf
    }
    

};

void progressBar::stopDisplaying(){
    if (coutRediret){ //remove redirect if it is still active
        coutRediret = false; //set redirect flag
        std::cout.rdbuf(stdout_buf); // reset std::cout to normal
        std::cout << "\n\n";
    }
};

void progressBar::updateCycleThreadFunction(){
while (showProgressBar){
    printProgress();
    std::this_thread::sleep_for(std::chrono::milliseconds(250));
}

};

void progressBar::progressType(eStoppingCondition condition){
    activeStoppingCondition = condition;
    updateBarPostfix();
};

void progressBar::completed(size_t completed){
    const std::lock_guard<std::mutex> lock(controll_mutex);
    //size_t is unsigned int, can't be negative, same as storedStoppingSamples, so no int underflow to set_progress.
    printSpinners();
    bar.set_progress((completed*100)/storedStoppingSamples);
    resetCursor();
};
void progressBar::stoppingSamples(size_t samples){
    storedStoppingSamples = samples;
};
void progressBar::stoppingTime(std::time_t time){
    storedStoppingTime = time;
};
void progressBar::startingTime(std::time_t time){
    storedStartingTime = time;
};
void progressBar::confidenceInterval(double ConfidenceIntervalWidth){
    const std::lock_guard<std::mutex> lock(controll_mutex);

    double progress = (log(ConfidenceIntervalWidth)*100)/storedLogStoppincCIWidth;
    progress = std::max(progress, 0.0); //prevent int underflow

    printSpinners();
    bar.set_progress((size_t) progress);
    resetCursor();
};
void progressBar::stoppingConfidenceInterval(double ConfidenceIntervalWidth){
    storedLogStoppincCIWidth = log(ConfidenceIntervalWidth);
};
void progressBar::workerStatuses(std::vector<eWorkerStatus> &statuses){
    const std::lock_guard<std::mutex> lock(controll_mutex);
    std::vector<activitySpinner> newSpinners(statuses.size());
    for(int i = 0; i < statuses.size(); i++){
        newSpinners[i].setTrackingThread(statuses, i);
    }
    spinners.swap(newSpinners);
};

void progressBar::enableDisplay(bool show){
    //change display state if needed
    if ((showProgressBar == false) && (show == true)){
        if (bar.is_completed()){
            resetBar();
        }
        showProgressBar = show; //update internal tracking of state
        startDisplaying();  //start cout redirect
        updateCycleThread = std::thread(&progressBar::updateCycleThreadFunction, this); //create auto update thread
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }
    if ((showProgressBar == true) && (show == false)) {
        showProgressBar = show; //update internal tracking of state
        updateCycleThread.join(); //collect created auto update thread (should halt on showProgressBar = false)
        printProgress();    //last completion print
        stopDisplaying();   //halt the cout redirect
    }

};

void progressBar::printSpinners(){
        os << "[ ";
    for(auto& s : spinners) {
            os << s.tickToString() + " ";
        }
    os << "]" ;

    os << "\n"; //move to next line to print bar
};

void progressBar::resetCursor(){
        if (bar.is_completed()){
            os << "\033[A"; //bar prints to new line after completion in stead of start of line, extra cursor up needed.
        }
        os << "\033[A"; //move cursor up a line, to spinner level after bar print
};

void progressBar::printProgress(){
    const std::lock_guard<std::mutex> lock(controll_mutex);
    printSpinners();
    if (activeStoppingCondition == eStoppingCondition::eTime){
        //could maybe optimize this, 
        //auto prog = ((std::time(nullptr))-storedStartingTime)*100/(storedStoppingTime-storedStartingTime);
        bar.set_progress(((std::time(nullptr))-storedStartingTime)*100/(storedStoppingTime-storedStartingTime));
    } else {
        bar.print_progress();
    }
    resetCursor();
}

};