#include "ActivitySpinner.h" 


namespace DFT
{
    activitySpinner::activitySpinner()
    {
        setTickSymbols(std::vector<std::string>{"|", "/", "-", "\\"});
    }

    void activitySpinner::tick()
    {   
        if (getTrackedThreadStatus() == eWorkerStatus::eWorking){
            progress = ((progress + 1) % tickSymbols.size());
        }
    }
    
    std::string activitySpinner::toString() const
    {
        std::string output;
        switch (getTrackedThreadStatus()){
            case eWorkerStatus::ePaused:
                output = "⏸";
                break;
            case eWorkerStatus::eStopping:
                output = "■";
                break;
            case eWorkerStatus::eStopped:
                output = "✓";
                break;
            default:
                output = tickSymbols[progress];
                break;
        }
        return output;
    }
    
    std::string activitySpinner::tickToString()
    {
        tick();
        return toString();
    }
    
    void activitySpinner::setTickSymbols(std::vector<std::string> newTickSymbols)
    {
        tickSymbols = newTickSymbols;
    }
    
    size_t activitySpinner::getProgress() const
    {
        return progress;
    }
    
    void activitySpinner::setProgress(size_t newProgress)
    {
        std::lock_guard<std::mutex> lock(set_mutex);
        progress = (newProgress % tickSymbols.size());
    }
    
    void activitySpinner::setTrackingThread(std::vector<eWorkerStatus> &threads, size_t const id)
    {
        std::lock_guard<std::mutex> lock(set_mutex);
        trackedThreadsVector = &threads;
        threadID = id;
    }
    

    eWorkerStatus activitySpinner::getTrackedThreadStatus() const
    {
        return trackedThreadsVector->at(threadID);
    }



}