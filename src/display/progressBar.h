#ifndef PROGRESS_BAR_H_
#define PROGRESS_BAR_H_


#include "enums/eStoppingCondition.h"
#include "enums/eWorkerStatus.h"
#include "ActivitySpinner.h"
#include "indicator.hpp"
#include <thread>
#include <mutex>
#include <chrono>
#include <boost/iostreams/filter/line.hpp>
#include <boost/iostreams/filtering_streambuf.hpp>


namespace DFT
{

class progressBar 
{
/*
 * C++ Singleton
 * Limitation: Single Threaded Design
 * See: http://www.aristeia.com/Papers/DDJ_Jul_Aug_2004_revised.pdf
 *      For problems associated with locking in multi threaded applications
 *
 * Limitation:
 * If you use this Singleton (A) within a destructor of another Singleton (B)
 * This Singleton (A) must be fully constructed before the constructor of (B)
 * is called.
 */

    private:

    // Private Constructor
        // Can't be constructed outside singleton
        progressBar();
        //Can't be destructed outside singleton, except for program termination
        ~progressBar();
        // Stop the compiler auto-generating methods that can copy the object
        progressBar(progressBar const& copy);            // Not Implemented
        progressBar& operator=(progressBar const& copy); // Not Implemented
        void resetBar();
        void updateBarPostfix();
        void startDisplaying();
        void stopDisplaying();
        void updateCycleThreadFunction();
        void printSpinners();
        void resetCursor();

        boost::iostreams::filtering_ostreambuf filtering_buf{};
        std::thread updateCycleThread;
        eStoppingCondition activeStoppingCondition;
        size_t storedStoppingSamples;
        std::time_t storedStoppingTime;
        std::time_t storedStartingTime;
        double storedLogStoppincCIWidth;
        std::streambuf *stdout_buf; //Stores the default stdout streambuf
        std::ostream os;    //Stream for the unfiltered progressBar output to write to
        std::mutex controll_mutex;
        bool coutRediret = false; //boolean tracking if cout is currently being redirected though the filter
        bool showProgressBar = false;
        std::vector<activitySpinner> spinners;
        indicators::ProgressBar bar;



public:
    static progressBar& getInstance()
        {
            // The only instance
            // Guaranteed to be lazy initialized
            // Guaranteed that it will be destroyed correctly
            static progressBar instance;
            return instance;
        }
    
    
    void progressType(eStoppingCondition condition);
    static void setProgressType(eStoppingCondition condition) {getInstance().progressType(condition);};
    void completed(size_t completed);
    static void updateCompleted(size_t completed) {getInstance().completed(completed);};
    void stoppingSamples(size_t samples);
    static void setStoppingSamples(size_t samples) {getInstance().stoppingSamples(samples);};
    void stoppingTime(std::time_t time);
    static void setStoppingTime(std::time_t time) {getInstance().stoppingTime(time);};
    void startingTime(std::time_t time);
    static void setStartingTime(std::time_t time) {getInstance().startingTime(time);};
    void confidenceInterval(double ConfidenceIntervalWidth);
    static void updateConfidenceInterval(double ConfidenceIntervalWidth) {getInstance().confidenceInterval(ConfidenceIntervalWidth);};
    void stoppingConfidenceInterval(double ConfidenceIntervalWidth);
    static void setStoppingConfidenceInterval(double ConfidenceIntervalWidth) {getInstance().stoppingConfidenceInterval(ConfidenceIntervalWidth);};
    void workerStatuses(std::vector<eWorkerStatus> &statuses);
    static void updateWorkers(std::vector<eWorkerStatus> &statuses) {getInstance().workerStatuses(statuses);};
    void enableDisplay(bool show);
    static void setProgressBars(bool show) {getInstance().enableDisplay(show);};
    static void showProgressBars() {setProgressBars(true);};
    static void hideProgressBars() {setProgressBars(false);};
    void printProgress();
    static void updateProgress() {getInstance().printProgress();};



};

}


#endif