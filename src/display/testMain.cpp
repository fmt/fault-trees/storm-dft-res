#include "progressBar.h"
#include "ActivitySpinner.h" 
#include "indicator.hpp"
#include "enums/eWorkerStatus.h"
#include <thread>
#include <chrono>
#include <boost/iostreams/filter/line.hpp>
#include <boost/iostreams/filtering_streambuf.hpp>

struct LineInsertingFilter : boost::iostreams::line_filter
{
    std::string do_filter(const std::string &line)
    {   //\n: generate next line/scroll down one line if it is on the bottom edge of the terminal
        //\033[A: move cursor up one line bach to the start of the line and
        //\033[L: move current line down one line in the empty space that is now guarenteed to exist
        return "\n\033[A\033[L" + line;
    }
};

// indicators::ProgressSpinner initSpinner(){
//     using namespace indicators;
//     return indicators::ProgressSpinner spinner{
//     option::PostfixText{"Checking credentials"},
//     option::ForegroundColor{Color::yellow},
//     option::SpinnerStates{std::vector<std::string>{"⠈", "⠐", "⠠", "⢀", "⡀", "⠄", "⠂", "⠁"}},
//     option::FontStyles{std::vector<FontStyle>{FontStyle::bold}}
//     };
// };

// int main(int argc, char *argv[])
// {   
//     using namespace indicators;
//     indicators::ProgressSpinner spinner{
//     option::PostfixText{"Checking credentials"},
//     option::ForegroundColor{Color::yellow},
//     option::SpinnerStates{std::vector<std::string>{"⠈", "⠐", "⠠", "⢀", "⡀", "⠄", "⠂", "⠁"}},
//     option::FontStyles{std::vector<FontStyle>{FontStyle::bold}}
//     };


//     // Update bar state
//     int n = 0;
//     while (true)
//     {
//         spinner.tick();
//         n += 10;
//         if ((n % 10) == 0)
//         {
//             std::cout << "n = " + std::to_string(n) << std::endl;
//         }
//         if (spinner.is_completed())
//             break;
//         std::this_thread::sleep_for(std::chrono::milliseconds(100));
//     }

//     return 0;
// }
    

// int main(int argc, char *argv[])
// {
//     auto *stdout_buf = std::cout.rdbuf(); // get stdout streambuf

//     // create a filtering_ostreambuf with our filter and the stdout streambuf as a sink
//     boost::iostreams::filtering_ostreambuf filtering_buf{};
//     filtering_buf.push(LineInsertingFilter());
//     filtering_buf.push(*stdout_buf);

//     std::cout.rdbuf(&filtering_buf); // configure std::cout to use our streambuf

//     std::ostream os(stdout_buf); // create local ostream acting as std::cout normally would

//     using namespace indicators;
//     ProgressBar bar{
//         option::Stream{os}, // use local ostream for progress bar
//         option::BarWidth{50},
//         option::Start{"["},
//         option::Fill{"="},
//         option::Lead{">"},
//         option::Remainder{" "},
//         option::End{"]"},
//         option::PostfixText{"Extracting Archive"},
//         option::ForegroundColor{Color::green},
//         option::FontStyles{std::vector<FontStyle>{FontStyle::bold}}};
//     std::vector<DFT::eWorkerStatus> statuses{DFT::eWorkerStatus::eWorking};
//     DFT::activitySpinner spinner;
//     spinner.setTrackingThread(statuses, 0);

//     // Update bar state
//     int n = 0;
//     while (true)
//     {
//         n += 1;
//         os << "[ " << spinner.tickToString() << " ]" << "\n";
//         bar.set_progress(n/10);
//         os << "\033[A";
//         if (bar.is_completed()) {
//             os << "\n";
//             std::cout.rdbuf(stdout_buf); // reset std::cout
//         }
//         if ((n % 100) == 0)
//         {
//             std::cout << "n = " + std::to_string(n) << std::endl;
//             os << "\n";
//             os.flush();
//             os << "\033[A";
//             os.flush();
//             os << "\033[L";
//             os.flush();
//             os << "n = " + std::to_string(n);
//             os.flush();
//             os << std::endl;
//             //std::cout << "n = " + std::to_string(n) << std::endl;
//         }
//         if (n == 500)
//         {
//             statuses[0] = DFT::eWorkerStatus::ePaused;
//         }
//         if (n == 800)
//         {
//             statuses[0] = DFT::eWorkerStatus::eStopping;
//         }
//         if (n == 900)
//         {
//             statuses[0] = DFT::eWorkerStatus::eStopped;
//         }
//         if (bar.is_completed())
//             break;
//         std::this_thread::sleep_for(std::chrono::milliseconds(10));
//     }

//     std::cout << "This line is printed normally." << std::endl;
    
//     return 0;
// }

int main(int argc, char *argv[])
{
    std::vector<DFT::eWorkerStatus> statuses{DFT::eWorkerStatus::eWorking};
    DFT::progressBar::updateWorkers(statuses);


    DFT::progressBar::setProgressType(DFT::eStoppingCondition::eTime);
    DFT::progressBar::setStartingTime(std::time(nullptr));
    time_t endingTime = std::time(nullptr) + 10;
    DFT::progressBar::setStoppingTime(endingTime);
    statuses[0] = DFT::eWorkerStatus::eWorking;
    DFT::progressBar::showProgressBars();
    time_t curTime = std::time(nullptr);
    //std::cout << std::to_string(curTime) << std::endl;
    
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    std::cout << "This line is printed with progress bars active." << std::endl;
    while ((std::time(nullptr)) < endingTime)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
    statuses[0] = DFT::eWorkerStatus::eStopped;
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    std::cout << "This line is printed with progress bars active." << std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    std::cout << "This line is printed with progress bars active." << std::endl;
    DFT::progressBar::hideProgressBars();
    std::cout << "This line is printed normally." << std::endl;


    int goal = 1000;
    statuses[0] = DFT::eWorkerStatus::eWorking;
    DFT::progressBar::setProgressType(DFT::eStoppingCondition::eSamples);
    DFT::progressBar::setStoppingSamples(goal);
    DFT::progressBar::updateCompleted(0);
    DFT::progressBar::showProgressBars();
    // Update bar state
    int n = 0;
    while (true)
    {
        n += 1;
        if ((n % 100) == 0)
        {
            DFT::progressBar::updateCompleted(n);
            std::cout << "n = " + std::to_string(n) << std::endl;
        }
        if (n == 5000)
        {
            statuses[0] = DFT::eWorkerStatus::ePaused;
        }
        if (n == 8000)
        {
            statuses[0] = DFT::eWorkerStatus::eStopping;
        }
        if (n == 9000)
        {
            statuses[0] = DFT::eWorkerStatus::eStopped;
        }
        if (n > goal)
            break;
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
    DFT::progressBar::hideProgressBars();
    std::cout << "This line is printed normally." << std::endl;

    double goalWidth = 0.0001;
    statuses[0] = DFT::eWorkerStatus::eWorking;
    DFT::progressBar::setProgressType(DFT::eStoppingCondition::eCI);
    DFT::progressBar::setStoppingConfidenceInterval(goalWidth);
    DFT::progressBar::updateConfidenceInterval(1);
    DFT::progressBar::showProgressBars();
    // Update bar state
    double width = 1;
    while (true)
    {
        width *= 0.3;
        DFT::progressBar::updateConfidenceInterval(width);
            std::cout << "width = " + std::to_string(width) << std::endl;
        if (width < goalWidth)
            break;
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }
    DFT::progressBar::hideProgressBars();
    std::cout << "This line is printed normally." << std::endl;

    return 0;
}