#include "logger.h"
#include <algorithm>
#include <iomanip>
#include <sstream>
#include <chrono>

namespace DFT
{

Logger::Logger(){
    logFile.open ("log_" + GetCurrentTimeForFileName() + ".txt");
    profilingFile.open("profiling_" + GetCurrentTimeForFileName() + ".txt");
};

Logger::~Logger(){
    logFile.close ();
    profilingFile.close();
}

std::string Logger::GetCurrentTimeForFileName()
{
    auto time = std::time(nullptr);
    std::stringstream ss;
    ss << std::put_time(std::localtime(&time), "%F_%T"); // ISO 8601 without timezone information.
    auto s = ss.str();
    std::replace(s.begin(), s.end(), ':', '-');
    return s;
};


std::string Logger::GetCurrentTimeForLog() {
  // get a precise timestamp as a string
  const auto now = std::chrono::system_clock::now();
  const auto nowAsTimeT = std::chrono::system_clock::to_time_t(now);
  const auto nowMs = std::chrono::duration_cast<std::chrono::milliseconds>(
      now.time_since_epoch()) % 1000;
  std::stringstream nowSs;
  nowSs
      << std::put_time(std::localtime(&nowAsTimeT), "%F_%T")
      << '.' << std::setfill('0') << std::setw(3) << nowMs.count();
  return nowSs.str();
}

// std::string Logger::GetCurrentTimeForLogOld()
// {
//     auto tim
// std::string Logger::GetCurrentTimeForLogOld()
// {
//     auto time = std::time(nullptr);
//     std::stringstream ss;
//     ss << std::put_time(std::localtime(&time), "%F_%T"); // ISO 8601 without timezone information.
//     auto s = ss.str();
//     return s;
// };

void  Logger::info(std::string const& msg){
    const std::lock_guard<std::mutex> lock(log_mutex);
    logFile << msg << std::endl;
};

void Logger::threadProgress(size_t id, size_t completed, double timeSeconds){
    const std::lock_guard<std::mutex> lock(profile_mutex);
    profilingFile << GetCurrentTimeForLog() << ",ThreadProgress," << id << "," << std::to_string(completed) << "," << std::to_string(timeSeconds) << std::endl;
};

void Logger::threadStatus(size_t id, eWorkerStatus status){
    const std::lock_guard<std::mutex> lock(log_mutex);
    logFile << GetCurrentTimeForLog() << " Thread: " << std::to_string(id) << " entered status: " << workerStatusToString(status) << std::endl;
};

}