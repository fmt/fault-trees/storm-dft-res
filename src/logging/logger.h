#ifndef LOGGER_H_
#define LOGGER_H_

#include <iostream>
#include <fstream>
#include <ctime>
#include <mutex>
#include "enums/eWorkerStatus.h"


namespace DFT
{

/*
 * C++ Singleton
 * Limitation: Single Threaded Design
 * See: http://www.aristeia.com/Papers/DDJ_Jul_Aug_2004_revised.pdf
 *      For problems associated with locking in multi threaded applications
 *
 * Limitation:
 * If you use this Singleton (A) within a destructor of another Singleton (B)
 * This Singleton (A) must be fully constructed before the constructor of (B)
 * is called.
 */
class Logger
{
    private:
        // Private Constructor
        // Can't be constructed outside singleton
        Logger();
        //Can't be destructed outside singleton, except for program termination
        ~Logger();
        // Stop the compiler auto-generating methods that can copy the object
        Logger(Logger const& copy);            // Not Implemented
        Logger& operator=(Logger const& copy); // Not Implemented
        
        std::ofstream logFile;
        std::ofstream profilingFile;
        static std::string GetCurrentTimeForFileName();
        static std::string GetCurrentTimeForLog();
        bool profiling = true;
        std::mutex log_mutex;
        std::mutex profile_mutex;

    public:
        static Logger& getInstance()
        {
            // The only instance
            // Guaranteed to be lazy initialized
            // Guaranteed that it will be destroyed correctly
            static Logger instance;
            return instance;
        }

        void info(std::string const& msg);
        static void logInfo(std::string const& msg) {getInstance().info(msg);};
        void warning(std::string const& msg);
        static void logWarning(std::string const& msg) {getInstance().warning(msg);};
        void error(std::string const& msg);
        static void logError(std::string const& msg) {getInstance().error(msg);};
        void threadProgress(size_t id, size_t completed, double timeSeconds);
        static void logThreadProgress(size_t id, size_t completed, double timeSeconds) {getInstance().threadProgress(id, completed, timeSeconds);};
        void threadStatus(size_t id, eWorkerStatus status);
        static void logThreadStatus(size_t id, eWorkerStatus status) {getInstance().threadStatus(id, status);};
        static bool profilingActive() {return getInstance().profiling;};
        static void setProfiling(bool active) {getInstance().profiling = active;};
};

}


#endif