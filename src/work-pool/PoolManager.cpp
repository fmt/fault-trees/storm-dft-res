#include "confidence-interval/ConfidenceIntervalBinomial.h"

#include "display/progressBar.h"
#include "logging/logger.h"
#include "PoolManager.h"
#include <algorithm>    // std::min
#include <chrono>       // time measurement
#include <ctime>

namespace DFT
{

PoolManager::PoolManager(){
    setFile("");
    setTimebound(0.00000000000000000000001);
    setCIPercentage(95.0);
    setWorkerCount(0);
    setStoppingCondition(eStoppingCondition::eSamples);
    setTargetSamples(1);
    setTargetThreadReturnTime(10);
    setCIBAlgorithm(eCIBType::eWilson);
    setPrintDebug(false);
};

void PoolManager::setFile(std::string const& file){
    const std::lock_guard<std::mutex> lock(run_mutex);
    std::cout << "Info: setting file to: " + file << std::endl;
    simFile = file;
};

void PoolManager::setTimebound(double timeboundInput){
    if (timeboundInput <= 0) {
        throw std::invalid_argument("Invalid timebound:" + std::to_string(timeboundInput) + ", must be non-zero positive.");
    }
    timebound = timeboundInput;
};

//sets number of workers. 
//Positive value sets workers to that ammount. 
//0 tries to autodetect from system. 
//Negative tries to keep that number of spare threads unoccupied.
//Auto/negative falls back to 1 in case of failure to detect number of system cores/keeping more cores free than exist
void PoolManager::setWorkerCount(int workers){
    const std::lock_guard<std::mutex> lock(run_mutex);
    enum class eThreadClass {
        eManual = 100,
        eAuto = 101,
        eSpare = 102,
    };
    eThreadClass threadType = eThreadClass::eAuto;
    if (workers < 0)
    {
        threadType = eThreadClass::eSpare;
    } else if (workers > 0)
    {
        threadType = eThreadClass::eManual;
    }
    
    //Atempt to autodetect number of cores/threads in system, returns 0 if it fails to detect.
    unsigned int nthreads = std::thread::hardware_concurrency();
    switch (threadType)
    {
    case eThreadClass::eManual:
        //If statement checks for positive int, unsigned int can be bigger than signed, cast should not cause issues
        nthreads = (unsigned int) workers;
        break;
    case eThreadClass::eSpare:
        {
            //If statement checks for negative int, 0 - workers should thus always result in non-zero positive int.
            //Unsigned int can be bigger than signed, cast should not cause issues
            unsigned int remove = (unsigned int)(0 - workers);
            //Prevent underflow (, max value of remove = nthreads)
            remove = std::min(remove, nthreads);
            nthreads = nthreads - remove;
        }
        break;
    //default behavior; auto.
    default:
        // if (nthreads > 4){
        //     nthreads -= 1;
        // }
        // if (nthreads > 8){
        //     nthreads -= 1;
        // }
        break;
    }
    //0 workers protection ()
    if (nthreads == 0){
            nthreads = 1;
        }
    workerCount = nthreads;
    std::vector<PoolWorker> newWorkers(workerCount);
    std::vector<eWorkerStatus> newStatus(workerCount, eWorkerStatus::eStopped);
    // Using swap to free memory on change
    threadsStatus.swap(newStatus);
    poolWorkers.swap(newWorkers);
    assignRandomSeedsToWotkers();
    DFT::progressBar::updateWorkers(threadsStatus);
};

void PoolManager::setStoppingCondition(eStoppingCondition condition){
    const std::lock_guard<std::mutex> lock(run_mutex);
    stoppingCondition = condition;
    DFT::progressBar::setProgressType(stoppingCondition);
};

void PoolManager::setStoppingCondition(std::string const& condition){
    setStoppingCondition(parseStoppingCondition(condition));
};

void PoolManager::setStoppingTime(std::time_t time){
    const std::lock_guard<std::mutex> lock(run_mutex);
    std::time_t now = std::time(nullptr);
    if (time < now) {
        std::cout << "Warning: Stopping time set in past, simulations will stop immediately!" << std::endl;
    };
    stoppingTime = time;
    DFT::progressBar::setStoppingTime(stoppingTime);
};

void PoolManager::setTargetCI(double width){
    const std::lock_guard<std::mutex> lock(run_mutex);
    stoppingCIWidth = width;
    DFT::progressBar::setStoppingConfidenceInterval(stoppingCIWidth);
};

void PoolManager::setTargetSamples(long int samples){
    const std::lock_guard<std::mutex> lock(run_mutex);
    if (!(samples>0)){
        std::cout << "Warning: Invalid sample count:" + std::to_string(samples) + ". simulations will stop immediately!" << std::endl;
    }
    stoppingSamples = samples;
    DFT::progressBar::setStoppingSamples(stoppingSamples);
};

//Sets the target amount of time a thread should run autonomously before returning his results/communicating with the manager, in seconds.
void PoolManager::setTargetThreadReturnTime(double returnTime){
const std::lock_guard<std::mutex> lock(run_mutex);
threadReturnTarget = returnTime;
};

void PoolManager::setCIPercentage(double percentage){
    const std::lock_guard<std::mutex> lock(run_mutex);
    ci.setConfidencePercentage(percentage);
};

void PoolManager::setCIBAlgorithm(eCIBType const& CIBType){
    const std::lock_guard<std::mutex> lock(run_mutex);
    ci.setActiveCIB(CIBType);
};

void PoolManager::setCIBAlgorithm(std::string const& CIBType){
    const std::lock_guard<std::mutex> lock(run_mutex);
    ci.setActiveCIB(CIBType);
};

bool PoolManager::stopCheck(size_t id, size_t workerCompletedSamples){
    //quick return if thread is already set to stopping
    if (threadsStatus[id] == eWorkerStatus::eStopping) {
        return threadsStatus[id] == eWorkerStatus::eStopping;
    }
    
    switch (stoppingCondition)
    {
        case eStoppingCondition::eTime:
            //No set progress needed for time: progress bar does time update automatically.
            {
                std::time_t now = std::time(nullptr);
                if (stoppingTime < now){
                    setThreadStatus(id, eWorkerStatus::eStopping);
                };
            }
            break;

        case eStoppingCondition::eSamples:
            {
                const std::lock_guard<std::mutex> lock(com_mutex);
                DFT::progressBar::updateCompleted(completedSamples);
                if (totalSamples() >= stoppingSamples){
                    setThreadStatus(id, eWorkerStatus::eStopping);
                };
                if (fixedAssignedSamples){
                    size_t unfinishedForWorker = (size_t) ((std::ceil((double)stoppingSamples / (double)workerCount))-workerCompletedSamples);
                    if (unfinishedForWorker == 0){
                        setThreadStatus(id, eWorkerStatus::eStopping);
                    }
                }
            }
            break;

        case eStoppingCondition::eCI:
            {
                bool narrowEnough;
                double lower_bound;
                double upper_bound;
                std::tie(lower_bound, upper_bound) = ci.ComputeBinominal(completedPositives, completedSamples);
                double CIWidth = (upper_bound - lower_bound);
                DFT::progressBar::updateConfidenceInterval(CIWidth);
                narrowEnough = (CIWidth <= stoppingCIWidth);
                if (narrowEnough){
                    pauseHoldingLoop(id);
                } else {
                    //If not narow enough, resume all paused workers
                    unpauseAllThreads();
                    stop_cv.notify_all();
                }
                
            }
            break;

        default:
        setThreadStatus(id, eWorkerStatus::eStopping);
        break;
    }
    return threadsStatus[id] == eWorkerStatus::eStopping; 
};

void PoolManager::pauseHoldingLoop(size_t id){
    //Loop to hold thread in while in paused state.
    //will set thread to stopping if all threads are halted (pause, stopping, stopped)
    //enter pause state by delaying stop check return (Stay in while loop, pause running with conditional_variable.wait())
    setThreadStatus(id, eWorkerStatus::ePaused);
    //if all workers halted (paused, stopping, stopped), stop permanently (return stopped true)
    if (allThreadsHalted()){
            stopAllThreads();
        }
    std::unique_lock<std::mutex> lock(stop_mutex);
    while (true){
         
        //If something changed the thread status away from paused, exit paused hold loop
        if (!threadPaused(id)){
            lock.unlock();
            stop_cv.notify_all();
            break;
        }
        //Releases lock on entering wait, reaquires on resume after notification.
        stop_cv.wait(lock, [&]{return !threadPaused(id);});
    }
}

void PoolManager::setRandomSeed(unsigned int seed)
{
    const std::lock_guard<std::mutex> lock(run_mutex);
    randomSeed = seed;
    assignRandomSeedsToWotkers();
    if (stoppingCondition != eStoppingCondition::eSamples){
        std::cerr << "WARNING! Reproducable results only available with 'number of samples' stopping condition!" << std::endl;
        fixedAssignedSamples = false;
    } else {
        fixedAssignedSamples = true;
    }
};

void PoolManager::assignRandomSeedsToWotkers(){
        for (size_t i = 0; i < workerCount; i++)
        {
            poolWorkers.at(i).setRandomSeed(randomSeed + (unsigned int) i);
        }
}

//Run_Mutex guards the settings and results to prevent outside modifications while running. 
//This mutex is included in all set functions, but is not needed in get-functions/reads.
void PoolManager::runSimulations(){
    resetResults();
    const std::lock_guard<std::mutex> lock(run_mutex);
    for (size_t i = 0; i < workerCount; i++)
        {
            if (printDebug){
                std::cout << "Debug: Manager started thread " + std::to_string(i)<< std::endl;
            }
            threads.push_back(std::thread(&PoolManager::threadFunction, this, std::ref(poolWorkers.at(i)), simFile, i));/* code */
        }
    DFT::progressBar::showProgressBars();
    for (auto &t : threads) {
        t.join();
        if (printDebug){
            std::cout << "Debug: Manager joined a thread."<< std::endl;
        }
    }
    DFT::progressBar::hideProgressBars();
};

//Threaded instructions controlling the PoolWorker objects.
void PoolManager::threadFunction(PoolWorker& worker, std::string const& file, size_t id){
    size_t noRuns;
    size_t count;
    size_t invalid;
    size_t threadID = id;
    setThreadStatus(threadID, eWorkerStatus::eWorking);
    if (printDebug){
        std::cout << "Debug: thread started, initial stop check: " + std::to_string(stopCheck(threadID, worker.getCompletedSampes()))<< std::endl;
        worker.setPrintDebug(printDebug);
    }
    while (!stopCheck(threadID, worker.getCompletedSampes())){
        noRuns = assignSamplesToWorker(worker.getCompletedSampes());
        if (noRuns == 0){
            setThreadStatus(threadID, eWorkerStatus::eStopping);
        }
        if (printDebug){
            std::cout << "Debug: thread performing runs: " + std::to_string(noRuns)<< std::endl;
        }
        
        auto t1 = std::chrono::high_resolution_clock::now();
        std::tie(count, invalid) = worker.simulateDft(file, timebound, noRuns);
        auto t2 = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double, std::milli> fp_ms = t2 - t1;

        if (printDebug){
            std::cout << "Debug: Manager Recieved: N " + std::to_string(noRuns) + ",P " + std::to_string(count) + ",I " + std::to_string(invalid) + ", in " + std::to_string(fp_ms.count()) + "ms."<< std::endl;
        }
        if (Logger::profilingActive()){
            Logger::logThreadProgress(id, noRuns, (fp_ms.count() / 1000));
        }
        if (invalid>0)
        {
            //Exception invalid trace detected
            throw std::invalid_argument("Invalid trace detected");
        }
        addResults(count, noRuns, fp_ms.count() / 1000);
    }
    setThreadStatus(threadID, eWorkerStatus::eStopped);
};

void PoolManager::setThreadStatus(size_t id, eWorkerStatus status){
    const std::lock_guard<std::mutex> lock(stop_mutex);
    threadsStatus[id] = status;
    if (Logger::profilingActive()){
        Logger::logThreadStatus(id, status);
    }
};

bool PoolManager::allThreadsHalted(){
    //Checks if all threads are paused, stopping, or stopped (!= eWorking)
    return all_of(threadsStatus.begin(), threadsStatus.end(), [&] (eWorkerStatus status) {return (status != eWorkerStatus::eWorking);});
};

bool PoolManager::threadPaused(size_t id){
    return threadsStatus[id]==eWorkerStatus::ePaused;
}

bool PoolManager::threadWorking(size_t id){
    return threadsStatus[id] == eWorkerStatus::eWorking;
}

bool PoolManager::threadStopping(size_t id) {
    return threadsStatus[id] == eWorkerStatus::eStopping;
}


void PoolManager::unpauseAllThreads(){
    const std::lock_guard<std::mutex> lock(stop_mutex);
    for (size_t i = 0; i < threadsStatus.size(); i++)
        {
            if (threadPaused(i)){
                threadsStatus[i] = eWorkerStatus::eWorking;
            }
        }
};

void PoolManager::stopAllThreads(){
    const std::lock_guard<std::mutex> lock(stop_mutex);
    for (size_t i = 0; i < threadsStatus.size(); i++)
        {
            if (threadPaused(i) || threadWorking(i)){
                threadsStatus[i] = eWorkerStatus::eStopping;
            }
        }
};

void PoolManager::addResults(long int positives, long int samples){
    const std::lock_guard<std::mutex> lock(com_mutex);
    runningSamples -= samples;
    completedSamples += samples;
    completedPositives += positives;
};

void PoolManager::addResults(long int positives, long int samples, double processingTime){
    const std::lock_guard<std::mutex> lock(com_mutex);
    runningSamples -= samples;
    completedSamples += samples;
    completedPositives += positives;
    totalProcessingTime += processingTime;
};

void PoolManager::resetResults(){
    const std::lock_guard<std::mutex> lock(run_mutex);
    runningSamples = 0;
    completedSamples = 0;
    completedPositives = 0;
    totalProcessingTime = 0;
};

double PoolManager::getProbability() const{
    return ((double) completedPositives / (double) completedSamples);
};

std::pair<double, double> PoolManager::getConfidenceInterval(){
    return ci.ComputeBinominal(completedPositives, completedSamples);
};

size_t PoolManager::assignSamplesToWorker(size_t workerCompletedSamples){
    const std::lock_guard<std::mutex> lock(com_mutex);
    //Initialize default number of samples (also max ever number of samples per batch)
    size_t assign = 1000000000;
        
    
    if (!fixedAssignedSamples){
        //default assign behavior
    
        
        if (stoppingCondition == eStoppingCondition::eSamples){
            //Estimate the number of jobs a single worker has to work on to finish all remaining samples, if all wokers get the same amount of jobs.
            size_t unfinishedPerWorker = (size_t) ((double)(stoppingSamples - completedSamples) / (double)workerCount);
            
            //The number of jobs no worker is working on yet.
            size_t unassigned = (size_t) (stoppingSamples - totalSamples());
            
            //Number of jobs to assign not exceed the number of unassigned jobs (remaining unfinished and unclaimed),
            assign = std::min(assign, unassigned);
            // or exceed estimated load per worker for uncompleted jobs (to ensure equal distribution of the not yet finised jobs).
            assign = std::min(assign, unfinishedPerWorker);
            
        
        }
        
    } else {


        //Fixed number of samples per worker behavior. Only guarenteed in case of eSamples stopping condition.
        if (stoppingCondition != eStoppingCondition::eSamples)
        {
            //Exception invalid code path detected.
            //fixedAssignedSamples was set True while it should not have been. 
            //The stopping condition is incompatible with fixed assigned samples.
            throw std::invalid_argument("Fixing samples per worker only guarentees consistency with total samples stopping condition");
        }

        size_t unfinishedForWorker = (size_t) ((std::ceil((double)stoppingSamples / (double)workerCount))-workerCompletedSamples);
        assign = std::min(assign, unfinishedForWorker);

    }

    //Incorporate time estimate to try and finish a batch every "threadReturnTarget" seconds.
    if (totalProcessingTime > 0 && completedSamples > 0) {
        size_t returnTimeTargetLimit = (size_t)1;
        //Estimate number of jobs that can be done within "threadReturnTarget" seconds by a thread
        returnTimeTargetLimit = (size_t) (((double)completedSamples/(double)totalProcessingTime)*threadReturnTarget);
        returnTimeTargetLimit = std::max(returnTimeTargetLimit, (size_t) 1);
        
        //Number of jobs should not exceed this limit
        assign = std::min(assign, returnTimeTargetLimit);

    } else {
        //If no info exists yet to estimate performance, start with 1 sample (only initialize workers and run single sample, assigned will "grow" to near equilibrium)
        assign = std::min(assign, (size_t)1);
    }
    
    //If any stage returns 0 for some reason, assign at least 1 sample, disabled in case of fixed assigned per worker behavior is required since it might interfere.
    //WARNING: will return 0 samples with fixed seed, stopCheck now checks for the edge case that would generate a 0 assign, (fixed traces per worker, worker finished) and halts before it happens.
    //In case of new edge cases, either include in stopCheck, or introduce a pause on assing == 0 mechanism.
    if (!fixedAssignedSamples){
        assign = std::max(assign, (size_t)1);
    }
    
    //Register this number of samples as currently running
    runningSamples += assign;

    //Actually assign them to thread
    return assign;
};

void PoolManager::setPrintDebug(bool active){
    const std::lock_guard<std::mutex> lock(run_mutex);
    printDebug = active;
};

};
