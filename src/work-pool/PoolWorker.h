#ifndef POOL_WORKER_H_
#define POOL_WORKER_H_

#include <storm-dft/api/storm-dft.h>
#include "storm-dft/simulator/DFTTraceSimulator.h"



namespace DFT
{
//check with https://epitools.ausvet.com.au/ciproportion
class PoolWorker

{
public: 
    PoolWorker();
    std::pair<double, double> simulateDft(std::string const& file, double timebound, size_t noRuns);
    double simulateDftProb(std::string const& file, double timebound, size_t noRuns);
    void setPrintDebug(bool active);
    size_t getCompletedSampes();
    void setRandomSeed(unsigned int);

protected:
    std::string activeFile;
    std::shared_ptr<storm::dft::simulator::DFTTraceSimulator<double>> simulator;
    std::shared_ptr<storm::dft::storage::DFT<double>> dft;
    std::shared_ptr<storm::dft::storage::DFTStateGenerationInfo> stateGenerationInfo;
    std::shared_ptr<boost::mt19937> gen;
    bool printDebug;
    size_t completedSamples = 0;
    unsigned int randomSeed;

};
};


/*
namespace {

    // Helper functions
    std::pair<double, double> simulateDft(std::string const& file, double timebound, size_t noRuns) {
        // Load, build and prepare DFT
        storm::dft::transformations::dft::DftTransformator<double> dftTransformator = storm::dft::transformations::dft::DftTransformator<double>();
        std::shared_ptr<storm::dft::storage::DFT<double>> dft = dftTransformator.transformBinaryFDEPs(*(storm::dft::api::loadDFTGalileoFile<double>(file)));
        EXPECT_TRUE(storm::dft::api::isWellFormed(*dft).first);

        // Set relevant events
        storm::dft::utility::RelevantEvents relevantEvents = storm::dft::api::computeRelevantEvents<double>(*dft, {}, {}, false);
        dft->setRelevantEvents(relevantEvents);

        // Find symmetries
        std::map<size_t, std::vector<std::vector<size_t>>> emptySymmetry;
        storm::dft::storage::DFTIndependentSymmetries symmetries(emptySymmetry);
        storm::dft::storage::DFTStateGenerationInfo stateGenerationInfo(dft->buildStateGenerationInfo(symmetries));
        
        // Init random number generator
        //storm::utility::setLogLevel(l3pp::LogLevel::TRACE);
        boost::mt19937 gen(5u);
        storm::dft::simulator::DFTTraceSimulator<double> simulator(*dft, stateGenerationInfo, gen);
        
        size_t count = 0;
        size_t invalid = 0;
        storm::dft::simulator::SimulationResult res;
        for (size_t i=0; i<noRuns; ++i) {
            res = simulator.simulateCompleteTrace(timebound);
            if (res == storm::dft::simulator::SimulationResult::SUCCESSFUL) {
                ++count;
            } else if (res == storm::dft::simulator::SimulationResult::INVALID) {
                // Discard invalid traces
                ++invalid;
            }
        }
        return std::make_pair(count, invalid);
    }

    double simulateDftProb(std::string const& file, double timebound, size_t noRuns) {
        size_t count;
        size_t invalid;
        std::tie(count, invalid) = simulateDft(file, timebound, noRuns);
        //EXPECT_EQ(invalid, 0ul);
        return (double) count / noRuns;
    }

    

}

*/
#endif // POOL_WORKER_H_
