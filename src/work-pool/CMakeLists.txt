
#specify ConfidenceInterval library
add_library(work-pool 
${CMAKE_CURRENT_SOURCE_DIR}/PoolManager.h
${CMAKE_CURRENT_SOURCE_DIR}/PoolManager.cpp 
${CMAKE_CURRENT_SOURCE_DIR}/PoolWorker.h
${CMAKE_CURRENT_SOURCE_DIR}/PoolWorker.cpp)

# Include complete src directory (and header files from Storm)
target_include_directories(work-pool PUBLIC ${PROJECT_SOURCE_DIR}/src ${storm_INCLUDE_DIR})
target_link_libraries(work-pool PRIVATE pthread confidence-interval storm-dft enums logging display)
