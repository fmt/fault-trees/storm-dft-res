#include "PoolWorker.h"
namespace DFT {


    PoolWorker::PoolWorker(){
        //std::string activeFile;
        //std::shared_ptr<storm::dft::simulator::DFTTraceSimulator<double>> simulator;
        setRandomSeed(5u);
        setPrintDebug(false);
    };

    // Helper functions
    std::pair<double, double> PoolWorker::simulateDft(std::string const& file, double timebound, size_t noRuns) {
        if (activeFile != file)
        {
            activeFile = file;
            // Load, build and prepare DFT
            dft = storm::dft::api::loadDFTGalileoFile<double>(file);
            storm::dft::transformations::DftTransformer<double> dftTransformer = storm::dft::transformations::DftTransformer<double>();
            dft = storm::dft::transformations::DftTransformer<double>::transformBinaryDependencies(*dft);
            if (!storm::dft::api::isWellFormed(*dft).first) {
                //Exception
            };

            // Set relevant events
            storm::dft::utility::RelevantEvents relevantEvents = storm::dft::api::computeRelevantEvents<double>(*dft, {}, {});
            dft->setRelevantEvents(relevantEvents, false);

            // Find symmetries
            std::map<size_t, std::vector<std::vector<size_t>>> emptySymmetry;
            storm::dft::storage::DFTIndependentSymmetries symmetries(emptySymmetry);
            stateGenerationInfo = std::make_shared<storm::dft::storage::DFTStateGenerationInfo>(dft->buildStateGenerationInfo(symmetries));
            
            // Init random number generator
            //storm::utility::setLogLevel(l3pp::LogLevel::TRACE);
            gen = std::make_shared<boost::mt19937>(randomSeed);
            simulator = std::make_shared<storm::dft::simulator::DFTTraceSimulator<double>>(*dft, *stateGenerationInfo, *gen);
            
        }

        if (!simulator) {
            // Check for nullptr
            throw std::invalid_argument("Simulator was not initialized!");
        }
        
        size_t count = 0;
        size_t invalid = 0;
        size_t negative = 0;
        storm::dft::simulator::SimulationResult res;
        for (size_t i=0; i<noRuns; ++i) {
            res = simulator->simulateCompleteTrace(timebound);
            if (res == storm::dft::simulator::SimulationResult::SUCCESSFUL) {
                ++count;
            } else if (res == storm::dft::simulator::SimulationResult::INVALID) {
                // Discard invalid traces
                ++invalid;
            } else if (res == storm::dft::simulator::SimulationResult::UNSUCCESSFUL) {
                //DEBUG: explicitly count unsuccessfull
                ++negative;
            }
        }
        completedSamples += noRuns;
        if (printDebug){
            std::cout << "Debug Worker Returns: N " + std::to_string(noRuns) + ",P " + std::to_string(count) + ",I " + std::to_string(invalid) + ",U " + std::to_string(negative) + ",C " + std::to_string(completedSamples)<< std::endl;
        }
        return std::make_pair(count, invalid);
    };

    double PoolWorker::simulateDftProb(std::string const& file, double timebound, size_t noRuns) {
        size_t count;
        size_t invalid;
        std::tie(count, invalid) = PoolWorker::simulateDft(file, timebound, noRuns);
        if (invalid>0)
        {
            //Exception invalid trace detected
        }
 
        return (double) count / noRuns;
    };

    void PoolWorker::setRandomSeed(unsigned int seed){
        randomSeed = seed;
    }

    void PoolWorker::setPrintDebug(bool active){
    printDebug = active;
    };

    size_t PoolWorker::getCompletedSampes(){
        return completedSamples;
    }

};
