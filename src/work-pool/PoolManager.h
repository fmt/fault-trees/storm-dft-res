#ifndef POOL_MANAGER_H_
#define POOL_MANAGER_H_

#include "confidence-interval/CI.h"

#include <thread>
#include <mutex>
#include <condition_variable>
#include "PoolWorker.h"
#include "enums/eStoppingCondition.h"
#include "enums/eCIBType.h"
#include "enums/eWorkerStatus.h"


namespace DFT
{
//check with https://epitools.ausvet.com.au/ciproportion
class PoolManager

{
public: 
    PoolManager();
    void setFile(std::string const& file);
    void setTimebound(double timebound);
    void setWorkerCount(int workers);
    void setStoppingCondition(eStoppingCondition condition);
    void setStoppingCondition(std::string const& condition);
    void setStoppingTime(std::time_t time);
    void setTargetCI(double width);
    void setTargetSamples(long int samples);
    void setTargetThreadReturnTime(double durationInSeconds);
    void setCIPercentage(double percentage);
    void setCIBAlgorithm(eCIBType const& CIBType);
    void setCIBAlgorithm(std::string const& CIBType);
    void resetResults();
    void runSimulations();
    double getProbability() const;
    std::pair<double, double> getConfidenceInterval();
    void setRandomSeed(unsigned int);
    void setPrintDebug(bool active);

private:
    std::string simFile;
    eStoppingCondition stoppingCondition;
    CI ci;
    std::vector<PoolWorker> poolWorkers;
    std::vector<std::thread> threads;
    std::vector<eWorkerStatus> threadsStatus;
    double timebound;
    std::time_t stoppingTime;
    long int stoppingSamples;
    double threadReturnTarget;
    double stoppingCIWidth;
    unsigned int workerCount;
    long int runningSamples;
    long int completedSamples;
    long int totalSamples(){return runningSamples + completedSamples;};
    long int completedPositives;
    double totalProcessingTime;
    std::mutex com_mutex;
    std::mutex run_mutex;
    std::mutex stop_mutex;
    std::condition_variable stop_cv;
    unsigned int randomSeed = (unsigned int)std::time(nullptr);
    bool printDebug;
    bool fixedAssignedSamples = false;

    bool stopCheck(size_t id, size_t completedSamples);
    void pauseHoldingLoop(size_t id);
    void threadFunction(PoolWorker& worker, std::string const& file, size_t id);
    void setThreadStatus(size_t id, eWorkerStatus status);
    bool allThreadsHalted();
    bool threadPaused(size_t id);
    bool threadWorking(size_t id);
    bool threadStopping(size_t id);
    void unpauseAllThreads();
    void stopAllThreads();
    void addResults(long int positives, long int samples);
    void addResults(long int positives, long int samples, double processingTime);
    size_t assignSamplesToWorker(size_t completedSamples);
    void assignRandomSeedsToWotkers();
    

};



};



#endif // POOL_MANAGER_H_