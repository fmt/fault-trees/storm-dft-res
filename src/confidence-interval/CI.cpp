#include <cmath>
#include <stdexcept>
#include <memory>
#include "ConfidenceIntervalBinomial.h"
#include "ConfidenceIntervalBinomialNormal.h"
#include "ConfidenceIntervalBinomialWilson.h"

#include "CI.h"

//DEBUG, REMOVE LATER
#include <iostream>

// Confidence interval controller, for calclating regular and binomial confidence intervals.
// https://en.wikipedia.org/wiki/Binomial_proportion_confidence_interval
namespace DFT {
    


CI::CI(double confidence_percentage, std::string const& CIBType) : eActiveCIB(eCIBType::eWilson), activeCIB(new ConfidenceIntervalBinomialWilson()) {
    //std::unique_ptr<ConfidenceInterval> activeCI;
    //CI::eCIType eActiveCI = eNormal;
    setConfidencePercentage(confidence_percentage);
    setActiveCIB(CIBType);
};


void CI::setActiveCIB(std::string const& CIBType){
    setActiveCIB(parseCIBType(CIBType));
};

void CI::setActiveCIB(eCIBType const& CIBType){
    if (eActiveCIB == CIBType) {
        // No change necessary
        return;
    }
    switch(CIBType){
        case eCIBType::eNormal:
            activeCIB.reset(new ConfidenceIntervalBinomialNormal());
            eActiveCIB = eCIBType::eNormal;
            break;
        //case eWilson:
        case eCIBType::eWilson:
            activeCIB.reset(new ConfidenceIntervalBinomialWilson());
            eActiveCIB = eCIBType::eWilson;
            break;
        default:
            throw std::invalid_argument("Unknown CIBType: " + CIBTypeToString(CIBType));
    };
};


void CI::setConfidencePercentage(double confidence_percentage){
    if (confidence_percentage>0 && confidence_percentage<100){
        if (confidence_percentage < 1) {
            std::cout << "Warning: unusually low percentage: " + std::to_string(confidence_percentage)
             + "%. Allowed percentage interval is (0, 100), not (0, 1)";
        };
        stored_percentage = confidence_percentage;
    }  else{
        throw std::invalid_argument("Invalid confidence percentage: " + std::to_string(confidence_percentage));
    };

};

//std::pair<double, double> CI::Compute(double mean, double std){
//    throw std::logic_error("Confidence interval for mean and standard deviation not implemented")
//};

std::pair<double, double> CI::ComputeBinominal(long int positives, long int samples){
    return (*activeCIB).Compute(positives, samples, stored_percentage); 
};


};