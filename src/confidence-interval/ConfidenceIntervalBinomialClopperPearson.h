#ifndef CONFIDENCE_INTERVAL_CLOPPER_PEARSON_H_
#define CONFIDENCE_INTERVAL_CLOPPER_PEARSON_H_

#include "ConfidenceIntervalBinomial.h"

namespace DFT
{

class ConfidenceIntervalBinomialClopperPearson : public virtual ConfidenceIntervalBinomial 
{
public:
    std::pair<double, double> Compute(long int mean, long int noRuns, double confidence_percentage) override;
private:
    double betaDistributionPQuartile(double p, double v, double w);
};
};
#endif // CONFIDENCE_INTERVAL_CLOPPER_PEARSON_H_