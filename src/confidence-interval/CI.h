#ifndef CONFIDENCE_INTERVAL_CONTROLLER_H_
#define CONFIDENCE_INTERVAL_CONTROLLER_H_

#include <string>
#include <memory>

#include "ConfidenceIntervalBinomial.h"
#include "../enums/eCIBType.h"

// Confidence interval controller, for calclating regular and binomial confidence intervals.
// https://en.wikipedia.org/wiki/Binomial_proportion_confidence_interval
namespace DFT 

{
    
class CI 

{
        
    public:
        CI(double confidence_percentage = 95, std::string const& CIBType = "Normal");
        void setActiveCIB(eCIBType const& CIBType);
        void setActiveCIB(std::string const& CIBType);
        void setConfidencePercentage(double confidence_percentage);
        

        //std::pair<double, double> Compute(double mean, double std);
        std::pair<double, double> ComputeBinominal(long int positives, long int samples);
    private:
        double stored_percentage;
        //std::unique_ptr<ConfidenceIntervalBinomial> activeCI;
        std::unique_ptr<ConfidenceIntervalBinomial> activeCIB;
        //eCIBType eActiveCI;
        eCIBType eActiveCIB;
    
};
    

};

#endif // CONFIDENCE_INTERVAL_CONTROLLER_H_
