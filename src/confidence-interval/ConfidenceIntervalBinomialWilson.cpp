#include <boost/math/special_functions/erf.hpp>
#include <cmath>
#include <stdexcept>
#include "ConfidenceIntervalBinomialWilson.h"


namespace DFT
{

std::pair<double, double> ConfidenceIntervalBinomialWilson::Compute(long int positives, long int samples, double confidence_percentage) {
    double mean = double(positives) / double(samples);
    double nSamples = double(samples);
    double zScore = z_score(confidence_percentage);
    double zSquared = pow(zScore, 2);
    double wilsonCenter = (1/ (1+ (zSquared/nSamples) ) ) * (mean + (zSquared/(2*nSamples) ) );
    double wilsonDeviation = (zScore/ (1+ (zSquared/nSamples) ) ) * sqrt( ((mean*(1-mean)) / nSamples) + (zSquared/(4*pow(nSamples,2)) ) );
    return std::make_pair(wilsonCenter - wilsonDeviation, wilsonCenter + wilsonDeviation);
};


};

