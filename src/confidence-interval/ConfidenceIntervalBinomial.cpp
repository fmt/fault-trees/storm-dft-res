#include <boost/math/special_functions/erf.hpp>
#include <cmath>
#include <stdexcept>
#include "ConfidenceIntervalBinomial.h"

namespace DFT
{

//std::pair<double, double> ConfidenceIntervalBinomial::Compute(double mean, double noRuns, double confidence_percentage);
//ConfidenceIntervalBinomial::ConfidenceIntervalBinomial();

double stored_percentage;

double stored_z_score;
    
double ConfidenceIntervalBinomial::z_score(double percentage){
    if (percentage != stored_percentage) {
        stored_percentage = percentage;
        //from https://stackoverflow.com/questions/65309212/how-to-convert-percentage-to-z-score-of-normal-distribution-in-c-c
        stored_z_score = sqrt(2) * boost::math::erf_inv(percentage / 100);
    };
    return stored_z_score;
};

ConfidenceIntervalBinomial::~ConfidenceIntervalBinomial(){ };

};
