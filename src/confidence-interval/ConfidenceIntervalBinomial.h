#ifndef CONFIDENCE_INTERVAL_H_
#define CONFIDENCE_INTERVAL_H_

#include <utility>

namespace DFT
{
//check with https://epitools.ausvet.com.au/ciproportion
class ConfidenceIntervalBinomial

{
public: 
    //ConfidenceIntervalBinomial();
    virtual std::pair<double, double> Compute(long int positives, long int samples, double confidence_percentage) = 0;
    virtual ~ConfidenceIntervalBinomial();

protected:
    double stored_percentage;
    double stored_z_score;
    double z_score(double percentage);

};
};



#endif // CONFIDENCE_INTERVAL_H_
