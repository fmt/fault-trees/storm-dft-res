#ifndef CONFIDENCE_INTERVAL_WILSON_H_
#define CONFIDENCE_INTERVAL_WILSON_H_

#include "ConfidenceIntervalBinomial.h"

namespace DFT
{

class ConfidenceIntervalBinomialWilson : public virtual ConfidenceIntervalBinomial 
{
public:
    std::pair<double, double> Compute(long int mean, long int noRuns, double confidence_percentage) override;
};
};
#endif // CONFIDENCE_INTERVAL_WILSON_H_