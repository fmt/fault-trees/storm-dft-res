#include <boost/math/special_functions/erf.hpp>
#include <cmath>
#include <stdexcept>
#include "ConfidenceIntervalBinomialNormal.h"


namespace DFT
{

std::pair<double, double> ConfidenceIntervalBinomialNormal::Compute(long int positives, long int samples, double confidence_percentage) {
    double mean = double(positives) / double(samples);
    double deviation = z_score(confidence_percentage) * sqrt( (mean * (1 - mean)) / samples);
    return std::make_pair(mean - deviation, mean + deviation);
};


};

