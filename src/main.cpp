#include <storm/api/storm.h>
#include <storm-parsers/api/storm-parsers.h>
#include <storm-parsers/parser/PrismParser.h>
#include <storm/storage/prism/Program.h>
#include <storm/storage/jani/Property.h>
#include <storm/modelchecker/results/CheckResult.h>
#include <storm/modelchecker/results/ExplicitQuantitativeCheckResult.h>
#include "storm-dft/settings/DftSettings.h"

#include <storm/utility/initialize.h>

#include <iostream>
#include <sstream>
#include "confidence-interval/CI.h"
#include "work-pool/PoolManager.h"
#include "cli-parser/CLI-Parser.h"
#include "enums/eStoppingCondition.h"
#include "enums/eCIBType.h"

bool test_CI(long int positives, long int samples, double confidence_percentage, DFT::eCIBType const& eCIBType, std::string lower_bound_test, std::string upper_bound_test){
    std::stringstream ss;
    DFT::CI CI;
    std::string lower_bound_result;
    std::string upper_bound_result;
    double lower_bound;
    double upper_bound;
    CI.setConfidencePercentage(confidence_percentage);
    CI.setActiveCIB(eCIBType);
    std::tie(lower_bound, upper_bound) = CI.ComputeBinominal(positives, samples);
    ss << std::fixed << std::setprecision(10) << lower_bound;
    lower_bound_result = ss.str();
    ss.str(std::string());
    ss << std::fixed << std::setprecision(10) << upper_bound;
    upper_bound_result = ss.str();
    ss.str(std::string());
    //std::cout << "[" + lower_bound_result + ", " + upper_bound_result + "]" << std::endl;
    return (lower_bound_result == lower_bound_test)&&(upper_bound_result == upper_bound_test);
}

void test_CI_Normal (){
    //Base truth provided by https://epitools.ausvet.com.au/ciproportion
    //1 in 1000 95% [-0.0009589838,0.0029589838]
    std::cout << "PassN1: " + std::to_string(test_CI(1,1000,95.0,DFT::eCIBType::eNormal,"-0.0009589838","0.0029589838")) << std::endl;
    //500 in 1000 95% [0.4690102484,0.5309897516]
    std::cout << "PassN2: " + std::to_string(test_CI(500,1000,95.0,DFT::eCIBType::eNormal,"0.4690102484","0.5309897516")) << std::endl;
    //1 in 1000 99% [-0.0015745411,0.5309897516]
    std::cout << "PassN3: " + std::to_string(test_CI(1,1000,99.0,DFT::eCIBType::eNormal,"-0.0015745411","0.0035745411")) << std::endl;
}

void test_CI_Wilson (){
    //Base Truth provided by https://epitools.ausvet.com.au/ciproportion
    //0 in 1000 95% [0.0000000000,0.0038267585]
    std::cout << "PassW1: " + std::to_string(test_CI(0,1000,95.0,DFT::eCIBType::eWilson,"0.0000000000","0.0038267585")) << std::endl;
    //1 in 1000 95% [0.0001765464,0.0056425586]
    std::cout << "PassW2: " + std::to_string(test_CI(1,1000,95.0,DFT::eCIBType::eWilson,"0.0001765464","0.0056425586")) << std::endl;
    //500 in 1000 95% [0.4690696004,0.5309303996]
    std::cout << "PassW3: " + std::to_string(test_CI(500,1000,95.0,DFT::eCIBType::eWilson,"0.4690696004","0.5309303996")) << std::endl;
    //1 in 1000 99% [0.0001174164,0.0084605662]
    std::cout << "PassW4: " + std::to_string(test_CI(1,1000,99.0,DFT::eCIBType::eWilson,"0.0001174164","0.0084605662")) << std::endl;
}

void CITests(){
    std::cout << "starting CI test\n";
    DFT::CI CI;
    double lower_bound;
    double upper_bound;
    long int positives = 500;
    long int samples = 1000;
    double confidence_percentage = 95.0;
    CI.setConfidencePercentage(confidence_percentage);
    CI.setActiveCIB(DFT::eCIBType::eWilson);
    std::tie(lower_bound, upper_bound) = CI.ComputeBinominal(positives, samples);
    //std::cout << "[" + std::to_string(lower_bound) + ", " + std::to_string(upper_bound) + "]" << std::endl;
    test_CI_Normal();
    test_CI_Wilson();
}

bool Test_Sim(std::string file, double timebound, long noRuns, double expected, double delta){
    DFT::PoolManager manager;
    manager.setFile(file);
    manager.setTimebound(timebound);
    manager.setWorkerCount(-2);
    manager.setCIPercentage(95.0);
    manager.setStoppingCondition(DFT::eStoppingCondition::eSamples);
    manager.setTargetSamples(noRuns);
    manager.setCIBAlgorithm(DFT::eCIBType::eWilson);
    manager.setPrintDebug(true);
    manager.runSimulations();
    double result = manager.getProbability();
    double lower_bound;
    double upper_bound;


    std::tie(lower_bound, upper_bound) = manager.getConfidenceInterval();
    
    std::stringstream ss;
    std::string lower_bound_result;
    std::string upper_bound_result;

    ss << std::fixed << std::setprecision(10) << lower_bound;
    lower_bound_result = ss.str();
    ss.str(std::string());
    ss << std::fixed << std::setprecision(10) << upper_bound;
    upper_bound_result = ss.str();
    ss.str(std::string());
    std::cout << "Confidence Interval:[" + lower_bound_result + ", " + upper_bound_result + "]" << std::endl;
    
    std::cout << "Result: " + std::to_string(result) + "%." << std::endl;
    std::cout << "Expected: " + std::to_string(expected) + "%." << std::endl;
    std::cout << "Deviation: " + std::to_string(delta) + "%." << std::endl;

    return (result >= (expected-delta))&&(result <= (expected+delta));
}
void SpareTest() {
    bool result;
    result = Test_Sim(STORM_TEST_RESOURCES_DIR "/dft/spare.dft", 1, 10000, 0.1118530638, 0.01);
    std::cout << "PassSim1: " + std::to_string(result) + "." << std::endl;
    result = Test_Sim(STORM_TEST_RESOURCES_DIR "/dft/spare2.dft", 1, 10000, 0.2905027469, 0.01);
    std::cout << "PassSim2: " + std::to_string(result) + "." << std::endl;
    result = Test_Sim(STORM_TEST_RESOURCES_DIR "/dft/spare3.dft", 1, 10000, 0.4660673246, 0.01);
    std::cout << "PassSim3: " + std::to_string(result) + "." << std::endl;
    result = Test_Sim(STORM_TEST_RESOURCES_DIR "/dft/spare4.dft", 1, 10000, 0.01273070783, 0.01);
    std::cout << "PassSim4: " + std::to_string(result) + "." << std::endl;
    result = Test_Sim(STORM_TEST_RESOURCES_DIR "/dft/spare5.dft", 1, 10000, 0.2017690905, 0.01);
    std::cout << "PassSim5: " + std::to_string(result) + "." << std::endl;
    result = Test_Sim(STORM_TEST_RESOURCES_DIR "/dft/spare6.dft", 1, 10000, 0.4693712702, 0.01);
    std::cout << "PassSim6: " + std::to_string(result) + "." << std::endl;
    result = Test_Sim(STORM_TEST_RESOURCES_DIR "/dft/spare7.dft", 1, 10000, 0.06108774525, 0.01);
    std::cout << "PassSim7: " + std::to_string(result) + "." << std::endl;
    result = Test_Sim(STORM_TEST_RESOURCES_DIR "/dft/spare8.dft", 1, 10000, 0.02686144489, 0.01);
    std::cout << "PassSim8: " + std::to_string(result) + "." << std::endl;
}

int main (int argc, char *argv[]) {
    //DEBUG: initialize settings for stand alone testing
    storm::dft::settings::initializeDftSettings("Storm-res", "storm-res");
    DFT::PoolManager manager;
    int response = DFT::Parser::parse(manager, argc, argv);
    if (response == 7357) {
        CITests();
        SpareTest();
    } else if (response == 0){
        manager.runSimulations();

        double result = manager.getProbability();
        double lower_bound;
        double upper_bound;


        std::tie(lower_bound, upper_bound) = manager.getConfidenceInterval();
        
        std::stringstream ss;
        std::string lower_bound_result;
        std::string upper_bound_result;

        ss << std::fixed << std::setprecision(10) << lower_bound;
        lower_bound_result = ss.str();
        ss.str(std::string());
        ss << std::fixed << std::setprecision(10) << upper_bound;
        upper_bound_result = ss.str();
        ss.str(std::string());
        std::cout << "Confidence Interval:[" + lower_bound_result + ", " + upper_bound_result + "]" << std::endl;
        
        std::cout << "Result: " + std::to_string(result) + "%." << std::endl;
    }

    return 0;
}
