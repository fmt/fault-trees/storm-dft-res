#ifndef E_WORKER_STATUS_H_
#define E_WORKER_STATUS_H_

#include <string>

namespace DFT
{

enum class eWorkerStatus {eWorking, ePaused, eStopping, eStopped};
static std::string workerStatusToString(eWorkerStatus status){
    std::string output;
    switch(status){
        case eWorkerStatus::eWorking:
            output = "working";
            break;
        case eWorkerStatus::ePaused:
            output = "paused";
            break;
        case eWorkerStatus::eStopping:
            output = "stopping";
            break;
        case eWorkerStatus::eStopped:
            output = "stopped";
            break;
    }
    return output;
};
}


#endif