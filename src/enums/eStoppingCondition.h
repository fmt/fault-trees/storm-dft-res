#ifndef E_STOPPING_CONDITION_H_
#define E_STOPPING_CONDITION_H_

#include <string>
#include <iostream>

namespace DFT
{

enum class eStoppingCondition {
    eTime = 100,
    eCI = 101,
    eSamples = 102,
    eError = 400
};
static eStoppingCondition parseStoppingCondition(std::string const& condition){
    if (condition == "time" || condition == "Time"|| condition == "T") return eStoppingCondition::eTime;
    if (condition == "samples" || condition == "Samples"|| condition == "S") return eStoppingCondition::eSamples;
    if (condition == "confidenceinterval" || condition == "ConfidenceInterval"|| condition == "confidenceInterval"|| condition == "CI") return eStoppingCondition::eCI;
    std::cout << "Warning: Cannot parse stoppig condition string: " + condition + ". Simulations will stop immediately!" << std::endl;
    return eStoppingCondition::eError;
};
static std::string stoppingConditionToString(eStoppingCondition stoppingCondition){
    std::string output;
    switch(stoppingCondition){
        case eStoppingCondition::eTime:
            output = "Time";
            break;
        case eStoppingCondition::eCI:
            output = "Confidence Interval";
            break;
        case eStoppingCondition::eSamples:
            output = "Samples";
            break;
        default:
            output = "stoppingConditionParseError";
            break;
    }
    return output;
};

}


#endif