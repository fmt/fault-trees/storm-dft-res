#ifndef E_CIB_Type_H_
#define E_CIB_Type_H_

#include <string>

namespace DFT 
{
    enum class eCIBType {eNormal, eWilson, eError};
    static eCIBType parseCIBType(std::string const& CIBType){
    if (CIBType == "Normal") return eCIBType::eNormal;
    if (CIBType == "Wilson") return eCIBType::eWilson;
    return eCIBType::eError;
    };
    static std::string CIBTypeToString(eCIBType CIBType){
        std::string output;
        switch(CIBType){
            case eCIBType::eNormal:
                output = "Normal approximation/Wald";
                break;
            case eCIBType::eWilson:
                output = "Wilson score";
                break;
            default:
                output = "CIBTypeParseError";
                break;
        }
        return output;
    };

}
        

#endif