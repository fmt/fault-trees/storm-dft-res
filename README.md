# Storm-dft-RES
Dynamic fault tree analysis via Monte Carlo simulation.

## Dependencies
Storm-dft-RES requires the `storm-dft` library of the [Storm modelchecker](https://www.stormchecker.org).
See the [documentation](https://www.stormchecker.org/getting-started.html) for details on how to install Storm.

## Build
Configure and compile the project by executing
```
mkdir build
cd build
cmake ..
make
```
The executable `storm-dft_-res` can then be found in the `build` directory.

To test whether everything was built successfully, execute
```
./storm-dft-res --run-tests
```

## Getting Started
A DFT can be analyzed via Monte Carlo simulation by executing the following command:
```
./storm-dft-res -I <DFT_FILE> -T <TIME_BOUND> -S T -R <RUN_TIME_LIMIT>
```

## Command line options
Storm-dft-res supports the following stopping criteria:
- Runtime limit: `-S T -R <RUN_TIME_LIMIT>` where `RUN_TIME_LIMIT` is given in seconds
- Fixed number of samples: `-S S -N <NO_SAMPLES>`
- Width of confidence interval: `-S CI --ci-width <WIDTH>`

The tool supports parallelization.
By default, the number of threads is chosen based on the number of available cores.
Use `-W <NO_THREADS>` to set the number of worker threads.
A negative number leaves the given number of threads free.

To obtain deterministic results, a random seed can be fixed via `--seed <SEED>`.
In addition, the stopping criteria must be a fixed number of samples.

For a complete list of commandline options, see
```
./storm-dft-res --help
```

# Authors
- Marijn Peppelman
- Matthias Volk
